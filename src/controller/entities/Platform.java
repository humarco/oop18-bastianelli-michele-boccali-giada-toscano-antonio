package controller.entities;

/**
 * The controller for a platform where the player jumps on
 */

import common.events.CollisionEvent;
import model.entities.PlatformModel;
import view.entities.EntityView;

/**
 * The platform controller.
 */
public class Platform extends AbstractEntity<PlatformModel> {

    /**
     * Entity platform.
     * 
     * @param model the platform model
     * @param view  the platform view
     */
    public Platform(final PlatformModel model, final EntityView view) {
        super(model, view);
    }

    @Override
    protected void handleCollisionEvent(final CollisionEvent collisionEvent) {
        // play sound
    }

}
