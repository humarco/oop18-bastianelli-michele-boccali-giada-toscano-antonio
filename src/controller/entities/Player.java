package controller.entities;

import com.google.common.eventbus.Subscribe;

import common.events.CollisionEvent;
import common.events.JumpEvent;
import common.events.MovementEvent;
import model.components.ComandableMovement;
import model.entities.PlayerModel;
import view.entities.EntityView;

/**
 * The player controller.
 */
public class Player extends AbstractEntity<PlayerModel> implements Comandable, Jumper {

    /**
     * Create a new player.
     * @param model the entity model
     * @param view the entity view
     */
    public Player(final PlayerModel model, final EntityView view) {
        super(model, view);
    }

    @Override
    protected void handleCollisionEvent(final CollisionEvent event) {

    }

    /* (non-Javadoc)
     * @see controller.entities.Comandable#handleInputEvent(common.events.MovementEvent)
     */
    @Subscribe
    public final void handleInputEvent(final MovementEvent event) {
        this.getModel().getComponent(ComandableMovement.class).move(event.getMovementValue());
    }

    /* (non-Javadoc)
     * @see controller.entities.Jumper#handleJumpEvent(common.events.JumpEvent)
     */
    @Subscribe
    public final void handleJumpEvent(final JumpEvent event) {

    }

}
