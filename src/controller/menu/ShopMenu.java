package controller.menu;

import common.MsgStrings;
import enumerators.SceneType;
import model.user.UserData;
import view.GenericView;
import view.menu.ShopView;

/**
 * Shop controller. It extends Controller interface and is register in the
 * EventBus. Create and handle the ShopView
 */
public class ShopMenu extends AbstractMenuController {

    private final ShopView view;

    /**
     * ShopMenu constructor.
     */
    public ShopMenu() {
        super();
        this.view = new ShopView(this);
        this.updateView();
    }

    @Override
    public final void sendMsg(final String msg) {
        switch (msg) {
        case MsgStrings.BUY_CHARACTER:
            getModel().getUser().addCharacter(view.getSelectedPlayer());
            getModel().getUser().subtractCoin(view.getSelectedPlayer().getCost());
            this.updateView();
            break;
        case MsgStrings.SET_CHARACTER:
            getModel().getUser().setCurrentCharacter(view.getSelectedPlayer());
            this.updateView();
            break;
        case MsgStrings.MENU:
            this.postSceneAndUnregister(SceneType.MENU);
            break;
        default:
            break;
        }
    }

    @Override
    public final GenericView getView() {
        return view;
    }

    private void updateView() {
        final UserData data = getModel().getUser();
        this.view.updateActivation(data.getCharacters(), data.getCurrentCharacter(), data.getCoin());

        this.view.setInfo(getModel().getUser().getCoin(), getModel().getUser().getMaxHeight());
    }

}
