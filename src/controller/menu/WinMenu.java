package controller.menu;

import common.MsgStrings;
import controller.GameControllerImpl;
import enumerators.Level;
import enumerators.SceneType;
import view.BackgroundMusic;
import view.GenericView;
import view.menu.WinView;

/**
 * The pause menu controller. This class create and handle the PauseView.
 */
public class WinMenu extends AbstractMenuController {

    private final WinView view;

    /**
     * PauseMenu constructor.
     */
    public WinMenu() {
        super();
        view = new WinView(this);
        this.view.setInfo(getModel().getUser().getCoin(), getModel().getUser().getMaxHeight());
        if (Level.values()[Level.values().length - 1 ].equals(getModel().getCurrentLevel().get())) {
            this.view.disableNextLevel();
        }
    }

    @Override
    public final void sendMsg(final String msg) {
        switch (msg) {
        case MsgStrings.RESUME:
            this.postSceneAndUnregister(SceneType.RESUME_GAME);
            break;
        case MsgStrings.SWITCH_AUDIO_MODE:
            BackgroundMusic.getInstance().switchAudioMode();
            break;
        case MsgStrings.MENU:
            GameControllerImpl.getInstance().gameOver();
            this.postSceneAndUnregister(SceneType.MENU);
            break;
        case MsgStrings.NEXT_LEVEL:
            getModel().setNextCurrentLevel();
            GameControllerImpl.getInstance().gameOver();
            this.postSceneAndUnregister(SceneType.NEW_GAME);
            break;
        default:
            break;
        }
    }

    @Override
    public final GenericView getView() {
        return view;
    }

}
