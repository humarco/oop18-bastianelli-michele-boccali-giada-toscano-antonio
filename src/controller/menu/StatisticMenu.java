package controller.menu;

import common.MsgStrings;
import enumerators.SceneType;
import model.user.CurrentUserImpl;
import view.GenericView;
import view.menu.StatisticsView;

/**
 * The statistic menu controller class. This class create a StatisticView and
 * manage the events.
 */
public class StatisticMenu extends AbstractMenuController {

    private final StatisticsView view;

    /**
     * StatisticMenu contructor.
     */
    public StatisticMenu() {
        super();
        this.view = new StatisticsView(this, CurrentUserImpl.getInstance().getUser().getPublicInfo());
    }

    @Override
    public final void sendMsg(final String msg) {
        if (msg.equals(MsgStrings.MENU)) {
            this.postSceneAndUnregister(SceneType.MENU);
        }
    }

    @Override
    public final GenericView getView() {
        return view;
    }

}
