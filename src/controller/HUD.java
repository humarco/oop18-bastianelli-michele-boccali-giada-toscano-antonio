package controller;

/**
 * Handles score, coins and the max height reached.
 */
public interface HUD {

    /**
     * HUD initialization method. It must be call to set HUD info.
     * 
     * @param maxHeight : initial max height reached
     * @param coins     : initial coin catched
     */
    void init(int maxHeight, int coins);

    /**
     * Add coins to the actual value.
     * 
     * @param coins to add.
     */
    void addCoins(int coins);

    /**
     * Remove coins to the actual value.
     * 
     * @param coins to remove.
     */
    void removeCoins(int coins);

    /**
     * Returns the coins the player already got and not saved.
     * 
     * @return coins available
     */
    int getCoins();

    /**
     * Sets the max height reached.
     * 
     * @param maxHeight - the max height the player has reached.
     */
    void setMaxHeight(int maxHeight);

    /**
     * Returns the greatest height the player has reached.
     * 
     * @return the max height reached
     */
    int getMaxHeight();

    /**
     * Returns the total coins the player has got.
     * @return total coins
     */
    int getTotalCoins();

    /**
     * Reset the temporary coin value to zero.
     */
    void resetTempCoins();

}
