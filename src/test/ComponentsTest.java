package test;

import static org.junit.Assert.assertFalse;

import org.jbox2d.common.Vec2;
import org.junit.Before;
import org.junit.Test;

import enumerators.PlayerCharacter;
import model.components.CoinValueImpl;
import model.components.LifeImpl;
import model.entities.PlayerModel;
import model.physics.BodyBuilderImpl;
import model.physics.Size2D;

public class ComponentsTest {

    private PlayerModel player;

    @Before
    public void setUp() {
        player = new PlayerModel(PlayerCharacter.TUX, BodyBuilderImpl.getInstance().allowedToSleep(false).gravity(1)
                .moveable(true).position(new Vec2(100, 100)).size(new Size2D(50, 50)).build());
    }

    @Test(expected = IllegalArgumentException.class)
    public void test1() {
        player.getComponent(CoinValueImpl.class);
    }

    @Test
    public void test2() {
        player.add(new LifeImpl(player, 1, 1, 1));
        player.getComponent(LifeImpl.class).setDead();

        assertFalse("Dead", player.getComponent(LifeImpl.class).isAlive());
    }
}
