package utils;

import java.util.Optional;
import java.util.Set;
import common.CommonStrings;
import model.user.UserImpl;
import model.user.UserProfile;

/**
 * Class with static method to handle a FileManager of Users.
 */
public final class FileUserManager {

    private FileUserManager() {
    }

    /**
     * Load all users from the login file.
     * 
     * @return a set of users or an empty set if no user is present
     */
    public static Set<UserImpl> load() {
        final FileManagerImpl<UserImpl> fm = new FileManagerImpl<UserImpl>();
        return fm.load(CommonStrings.LOGIN_FILE);
    }

    /**
     * Update into the file user information or add it if User don't exist. Then,
     * file is saved.
     * 
     * @param user : the user to be update or add into the file
     */
    public static void update(final UserImpl user) {
        final Set<UserImpl> users = FileUserManager.load();
        FileUserManager.searchUsername(users, user.getUserProfile().getUsername()).ifPresent(e -> {
            users.remove(e);
        });
        users.add(user);
        FileUserManager.save(users);
    }

    /**
     * Save set of user into the file. It rewrite the entire file.
     * 
     * @param users : the users set to be saved into the file
     */
    public static void save(final Set<UserImpl> users) {
        final FileManager<UserImpl> fm = new FileManagerImpl<>();
        fm.save(CommonStrings.LOGIN_FILE, users);
    }

    /**
     * Search if a profile is present in the given set of User.
     * 
     * @param users   : the set of users
     * @param profile : profile to be search
     * @return true if username and password match
     */
    public static Optional<UserImpl> searchUserProfile(final Set<UserImpl> users, final UserProfile profile) {
        for (final UserImpl user : users) {
            if (user.getUserProfile().getUsername().equals(profile.getUsername())
                    && (user.getUserProfile().getPassword().equals(profile.getPassword()))) {
                return Optional.of(user);
            }
        }
        return Optional.empty();
    }

    /**
     * Search if the username is present in the given set of User.
     * 
     * @param users    : the set of users
     * @param username : username to be search
     * @return true if the username is already present
     */
    public static Optional<UserImpl> searchUsername(final Set<UserImpl> users, final String username) {
        for (final UserImpl user : users) {
            if (user.getUserProfile().getUsername().equals(username)) {
                return Optional.of(user);
            }
        }
        return Optional.empty();
    }
}
