package factories;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import org.jbox2d.common.Vec2;

import controller.entities.Coin;
import enumerators.CoinType;
import model.entities.CoinModel;
import model.physics.BodyBuilderImpl;
import model.physics.Size2D;
import view.entities.EntityView;

/**
 * Factory to generate all the coins.
 */
public class FactoryCoin extends GenericFactory<CoinType, CoinModel> {

    private static final Map<CoinType, Supplier<CoinModel>> COIN_MAP = new HashMap<>();
    static {
        COIN_MAP.put(CoinType.SIMPLE, () -> new SimpleCoin(getPos()));
    }

    /**
     * Create a coin.
     * 
     * @param type     the coin type
     * @param position the position where to place
     * @return the coin controller
     */
    protected final Coin createCoin(final CoinType type, final Vec2 position) {
        final CoinModel model = createCoinModel(type, position);
        final EntityView view = createView(model);
        return new Coin(model, view);
    }

    /**
     * Create a coin model.
     * @param type the coin character
     * @param position the position where to place
     * @return the coin model
     */
    protected final CoinModel createCoinModel(final CoinType type, final Vec2 position) {
        return createEntityModel(type, position, COIN_MAP);
    }

    /**
     * Simple coin specialization of Coin model.
     */
    private static final class SimpleCoin extends CoinModel {

        private static final CoinType C_TYPE = CoinType.SIMPLE;

        SimpleCoin(final Vec2 position) {
            super(C_TYPE,
                    BodyBuilderImpl.getInstance()
                                    .allowedToSleep(true)
                                    .size(new Size2D(C_TYPE.getWidth(), C_TYPE.getHeight()))
                                    .position(position)
                                    .moveable(false)
                                    .solid(true)
                                    .restitution(0)
                                    .gravity(0)
                                    .build());
            this.addDefaultCoinValue();
            this.addDefaultLife();
        }
    }

}
