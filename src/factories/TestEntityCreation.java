package factories;

import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

import enumerators.PlayerCharacter;
import enumerators.SpecificType;
import model.entities.PlayerModel;

public class TestEntityCreation {

    public enum BadType implements SpecificType {
        BAD("bad.png", 50, 50);

        private String imageName;
        private int width;
        private int height;

        private BadType(String imageName, int width, int height) {
            this.imageName = imageName;
            this.width = width;
            this.height = height;
        }

        @Override
        public String getImageName() {
            return imageName;
        }

        @Override
        public int getWidth() {
            return width;
        }

        @Override
        public int getHeight() {
            return height;
        }

    }

    private FactoryPlayer fPlayer;

    @Before
    public void setUp() {
        fPlayer = new FactoryPlayer();
    }

    @Test(expected = IllegalArgumentException.class)
    public void test3() {
        final PlayerModel pModel = fPlayer.createPlayerModel(PlayerCharacter.BIRD, null);
        assertNull(pModel);
    }

    // Dopo aver visto il pericolo dell'eccezione le classi interessate sono state
    // modificate
    /*
     * @Test(expected = IllegalArgumentException.class) public void test1() {
     * EnemyModel enemyModel = (EnemyModel) fEnemy.createModel(BadType.BAD, new
     * Vec2(100, 100)); } /*
     * 
     * /*
     * 
     * @Test(expected = ClassCastException.class) public void test2() { CoinModel
     * coinModel = (CoinModel) fEnemy.createModel(EnemyCharacter.GOOMBA, new
     * Vec2(100, 100)); }
     */
}
