package model.user;

import java.util.Optional;

import enumerators.Level;

/**
 * Current user interface.
 */
public interface UserModel {

    /**
     * Returns the persistent informations of the current User.
     * 
     * @return the user persistent information
     */
    UserImpl getUser();

    /**
     * Set user's persistent informations in the current user.
     * 
     * @param user : the user to set as Current User
     */
    void setUser(UserImpl user);

    /**
     * Returns an optional describing the level to play, if not-null. Otherwise
     * return an empty Optional
     * 
     * @return an optional describing the level to play, if not-null. Otherwise
     *         return an empty Optional.
     */
    Optional<Level> getCurrentLevel();

    /**
     * Set the level to play.
     * 
     * @param currentLevel : the Level to play
     */
    void setCurrentLevel(Level currentLevel);

    /**
     * Set next current level if possible.
     */
    void setNextCurrentLevel();

}
