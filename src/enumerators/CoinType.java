package enumerators;

/**
 * Contain all the coin types of the game.
 */
public enum CoinType implements SpecificType {
    /**
     * Simple coin, gives 1 coin.
     */
    SIMPLE("coin.png", 40, 40, 1);

    private String imageName;
    private int width;
    private int height;
    private int value;

    /**
     * Coin type enumerator.
     * @param imageName - path to the coin image.
     * @param width - coin width
     * @param height - coin height
     * @param value - coin value
     */
    CoinType(final String imageName, final int width, final int height, final int value) {
        this.imageName = imageName;
        this.width = width;
        this.height = height;
        this.value = value;
    }

    @Override
    public String getImageName() {
        return imageName;
    }

    /**
     * Returns the width of the coin.
     * @return coin width
     */
    public int getWidth() {
        return width;
    }

    /**
     * Returns the height of the coin.
     * @return coin height
     */
    public int getHeight() {
        return height;
    }

    /**
     * Returns the value of the coin.
     * @return coin value
     */
    public int getValue() {
        return value;
    }

}
