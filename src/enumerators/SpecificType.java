package enumerators;

/**
 * Interface base type for all the characters of the game.
 */
public interface SpecificType {

    /**
     * @return the image name
     */
    String getImageName();

    /**
     * @return the character's width
     */
    int getWidth();

    /**
     * @return the character's height
     */
    int getHeight();

}
