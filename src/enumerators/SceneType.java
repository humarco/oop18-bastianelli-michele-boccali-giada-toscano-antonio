package enumerators;

/**
 * Enumeration of scene type.
 */
public enum SceneType {
    /**
     * Login scene.
     */
    LOGIN, 
    /**
     * Menu scene.
     */
    MENU, 
    /**
     * New Game scene.
     */
    NEW_GAME,
    /**
     * Game menu scene.
     */
    GAME_MENU,
    /**
     * Statistic scene.
     */
    STATISTICS,
    /**
     * Shop scene.
     */
    SHOP,
    /**
     * End Game scene.
     */
    END_GAME,
    /**
     * Resume scene.
     */
    RESUME_GAME,
    /**
     * Win game scene.
     */
    WIN_GAME;
}
