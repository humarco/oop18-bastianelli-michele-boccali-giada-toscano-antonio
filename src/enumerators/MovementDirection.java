package enumerators;

/**
 * Enumerators for the movement directions.
 */
public enum MovementDirection {
    /**
     * Movement to the right.
     */
    RIGHT, 
    /**
     * Movement to the left.
     */
    LEFT;
}
