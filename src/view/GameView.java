package view;

import enumerators.Level;
import javafx.collections.ObservableList;
import javafx.geometry.Dimension2D;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.Scene;

public interface GameView extends GenericView {

    /**
     * Initializes the level and sets the background based on it.
     * @param level - the selected level
     */
    void init(Level level);

    /**
     * Returns the children of the root of the scene.
     * 
     * @return the ObservableList of the root nodes.
     */
    ObservableList<Node> getViewNodes();

    /**
     * Returns the view Dimension2D.
     * 
     * @return view Dimension2D
     */
    Dimension2D getViewDimension();

    /**
     * Returns the game camera.
     * @return the camera
     */
    Camera getCamera();

    /**
     * Starts the view timer.
     */
    void timerStart();

    /**
     * Stops the view timer.
     */
    void timerStop();

    /**
     * Returns the camera position.
     * 
     * @return the camera position
     */
    Point2D getCameraPosition();

    /**
     * Returns the JavaFX scene.
     * @return Scene
     */
    Scene getScene();

    /**
     * Set score info.
     * @param coins - the total coins of this game.
     * @param maxHeight - max height reached.
     */
    void setInfo(int coins, int maxHeight);

}