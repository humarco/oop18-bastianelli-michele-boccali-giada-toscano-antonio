package view;

/**
 * Scene switcher interface. This class set the actual view.
 */
public interface SceneSwitcher {

    /**
     * Set the given view scene as current scene.
     * 
     * @param view : the view to set as current view
     */
    void setScene(GenericView view);

}
