package view.menu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import common.CommonStrings;
import common.MsgStrings;
import controller.menu.Controller;
import enumerators.Level;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import view.ScoreView;

/**
 * Create a concrete main menu view. Extends a GenericView.
 */
public class MainMenuView extends ScoreView {

    private static final Double TOP = CommonStrings.WINDOW_WIDTH / 100.0 * 15;
    private static final Double BOTTOM = CommonStrings.WINDOW_WIDTH / 100.0 * 20;
    private static final Double LATERAL = 0.0;
    private static final String WELCOME = "Welcome ";

    private final Label title;

    /**
     * MainMenuView constructor.
     * 
     * @param c      : MainManu controller
     * @param levels : number of unlocked level
     */
    public MainMenuView(final Controller c, final int levels) {
        super(c);
        final BorderPane borderPane = new BorderPane();
        super.init(borderPane, TOP, BOTTOM, LATERAL, LATERAL);

        title = new Label();
        title.setId("title");
        final VBox titleBox = new VBox();
        titleBox.getChildren().addAll(title, super.getHBox());

        borderPane.setTop(titleBox);

        // center
        final List<Button> levelBtns = createLevelButtons(levels);
        final TilePane levelBox = new TilePane();

        final TilePane optionsPane = new TilePane();

        final Button statisticBtn = new MsgEventButton(this, MsgStrings.STATISTICS).getButton();
        final Button shopBtn = new MsgEventButton(this, MsgStrings.SHOP).getButton();
        final Button audioBtn = new AudioEventButton(this).getButton();

        optionsPane.getChildren().addAll(statisticBtn, shopBtn, audioBtn);

        levelBox.getChildren().addAll(levelBtns);

        borderPane.setCenter(levelBox);
        borderPane.setBottom(optionsPane);
    }

    private List<Button> createLevelButtons(final int unlocked) {
        final List<Button> buttons = new ArrayList<Button>();
        Arrays.asList(Level.values()).forEach(e -> {
            final Button b = new MsgEventButton(this, e.toString()).getButton();
            buttons.add(b);
            if (e.ordinal() > unlocked) {
                b.setDisable(true);
            }
        });
        return buttons;
    }

    /**
     * Method to set the name of the player.
     * 
     * @param playerName : title to add
     */
    public void setTitle(final String playerName) {
        this.title.setText(WELCOME + playerName);
    }

}
