package view.menu;

import common.CommonStrings;
import common.MsgStrings;
import controller.menu.Controller;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import view.GenericViewImpl;

/**
 * Create a concrete login view. Extends a GenericView.
 */
public class LoginView extends GenericViewImpl {

    private static final Double TOP = CommonStrings.WINDOW_WIDTH / 100.0 * 15;
    private static final Double BOTTOM = CommonStrings.WINDOW_WIDTH / 100.0 * 20;
    private static final Double LATERAL = CommonStrings.WINDOW_WIDTH / 100.0 * 10;
    private final TextField usernameField;
    private final PasswordField passwordField;
    private final Label error;

    /**
     * LoginView constructor.
     * 
     * @param c : Login controller
     */
    public LoginView(final Controller c) {
        super(c);
        final BorderPane borderPane = new BorderPane();
        super.init(borderPane, TOP, BOTTOM, LATERAL, LATERAL);

        // title
        final Label title = new Label("Jump Mania");
        title.setId("title");
        final HBox titleBox = new HBox();
        titleBox.getChildren().add(title);
        borderPane.setTop(titleBox);

        // central
        final VBox textBox = new VBox();

        final Label username = createLabel("Username");
        usernameField = new TextField();

        final Label password = createLabel("Password");
        passwordField = new PasswordField();

        error = new Label();
        error.setVisible(false);
        error.setId("error-label");

        textBox.getChildren().addAll(username, usernameField, password, passwordField, error);
        borderPane.setCenter(textBox);

        // bottom
        final HBox buttonBox = new HBox();

        final Button loginBtn = new MsgEventButton(this, MsgStrings.LOGIN).getButton();
        final Button newProfileBtn = new MsgEventButton(this, MsgStrings.REGISTER).getButton();

        buttonBox.getChildren().addAll(loginBtn, newProfileBtn);
        borderPane.setBottom(buttonBox);
    }

    private Label createLabel(final String name) {
        return new Label(name);
    }

    /**
     * Returns the given input username.
     * 
     * @return the username
     */
    public String getUsername() {
        return usernameField.getText();
    }

    /**
     * Returns the given input password.
     * 
     * @return the password
     */
    public String getPassword() {
        return passwordField.getText();
    }

    /**
     * Set the error message and set it visible.
     * 
     * @param errorMsg : String with the error message
     */
    public void setErrorMsg(final String errorMsg) {
        error.setText(errorMsg);
        error.setVisible(true);
    }
}
