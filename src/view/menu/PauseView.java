package view.menu;

import java.util.ArrayList;
import java.util.List;

import common.MsgStrings;
import controller.menu.Controller;
import javafx.scene.control.Button;

/**
 * Create a concrete pause view. Extends a RowsView.
 */
public class PauseView extends RowsView {

    /**
     * PauseView constructor.
     * 
     * @param c : Pause controller
     */
    public PauseView(final Controller c) {
        super(c);
    }

    @Override
    public final String setTitle() {
        return MsgStrings.PAUSE;
    }

    @Override
    public final List<Button> setButtons() {
        final List<Button> list = new ArrayList<Button>();
        list.add(new MsgEventButton(this, MsgStrings.MENU).getButton());
        list.add(new MsgEventButton(this, MsgStrings.RESUME).getButton());
        list.add(new AudioEventButton(this).getButton());
        return list;
    }

}
