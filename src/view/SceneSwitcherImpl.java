package view;

import common.CommonStrings;
import javafx.stage.Stage;

/**
 * This class contains the main stage view setting.
 */
public class SceneSwitcherImpl implements SceneSwitcher {

    private final Stage stage;

    /**
     * Public constructor.
     * 
     * @param stage : the main stage
     */
    public SceneSwitcherImpl(final Stage stage) {
        super();
        this.stage = stage;
        stage.setResizable(false);
        this.stage.setWidth(CommonStrings.WINDOW_WIDTH);
        this.stage.setHeight(CommonStrings.WINDOW_HEIGHT);
    }

    /**
     * Set the given view scene as current scene.
     * 
     * @param view : the view to set as current view
     */
    @Override
    public void setScene(final GenericView view) {
        this.stage.setScene(view.getScene());
        this.stage.show();
    }
}
